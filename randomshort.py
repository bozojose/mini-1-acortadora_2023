#!/usr/bin/python3

import webapp
import random
import string
import shelve

from urllib.parse import unquote

urlDic = shelve.open('urlDic')  # Diccionario para las URLs con llave una cadena aleat --> '/recurso': 'URL'
urlDic_inverso = shelve.open('urlDic_inverso')  # Diccionario con clave URL y valor cadena aleat --> 'URL' : 'recurso'

formulario = """
 <form action ="/" method ="POST">
    <h2>Introduzca una URL:</h2>
    Url: <input type="text" name="url"><br><br>
    <input type="submit" value="Acortar"><br>
 </form>
"""

respuesta_200_get = """
<html><body><h1>Acortador de URLs </h1>{formulario}<br><br>URLs almacenadas: <ol>{mensaje}</ol></body></html>
"""

respuesta_200_post = """
<html><body><h1>URL acortada: 
<a href=http://localhost:1234/{urlDic_inv_contenido}>http://localhost:1234/{urlDic_inv_contenido}</a>
</h1><h2>URL original: <a href={contenido}>{contenido}</a></h2><br>{formulario}<br>
<br>URLs almacenadas: <ol>{mensaje}</ol><br><br><a href=http://localhost:1234/>Volver</a></body></html>
"""

respuesta_400 = """
<html><body><h1>Por favor, introduzca una URL en el formulario.</h1>
{formulario}<br><br>URLs almacenadas: <ol>{mensaje}</ol>
<br><br><a href=http://localhost:1234/>Volver</a></body></html>
"""

respuesta_404 = """
<html><body><h1>Recurso {resource} no encontrado.</h1>
{formulario}<br><br>URLs almacenadas: <ol>{mensaje}</ol><br><br>
<a href=http://localhost:1234/>Volver</a></body></html>
"""

respuesta_405 = "405 Method Not Allowed"


class Acortador_URLs(webapp.webApp):
    def parse(self, request):
        print(request)
        if request == '':
            method = ''
            resource = ''
            body = ''
        else:
            method, resource, _ = request.split(' ', 2)  # Trocea los dos primeros espacios en blanco y
            # luego guarda las 2 primeras palabras
            body = unquote(request.split('\r\n\r\n', 1)[1])  # Trocea por /r/n/r/n solo una vez y
        # acaba (almacena el segundo campo, que sera el body, ya decodificado con utf-8 con unquote)
        return method, resource, body

    def process(self, parsedRequest):
        method, resource, body = parsedRequest
        print('\r\nEl metodo es: ' + method)
        print('El recurso es: ' + resource)
        print('El cuerpo es: ' + body)

        if method == '':
            return (respuesta_405, "")

        if method == "GET":
            mensaje = ""
            for clave, valor in urlDic.items():  # para cada elemento del dic persistente se
                # obtienen sus claves y valores para poder listarlos en la respuesta
                mensaje += "<ul><a href=http://localhost:1234" + str(clave) + ">" +\
                           str(clave) + "</a>: " + str(valor) + "</ul>"

            if resource == "/":  # Recurso / (el recurso principal)
                return ("200 OK", respuesta_200_get.format(formulario=formulario, mensaje=mensaje))
            else:
                if resource in urlDic:  # Si el recurso ya esta como clave, entonces
                    # se hace una redireccion (para URLs ya acortadas)
                    print("HTTP REDIRECT: " + str(urlDic[resource]))
                    return ("302 Found\r\nLocation: " + urlDic[resource], "")  # Redirección
                else:  # Resto de recursos (de localhost) que no estan disponibles
                    return ("404 NOT FOUND", respuesta_404.format(resource=str(resource),
                            formulario=formulario, mensaje=mensaje))

        if method == "POST":
            mensaje = ""
            for clave, valor in urlDic.items():  # para cada elemento del dic persistente se
                # obtienen sus claves y valores para poder listarlos en la respuesta
                mensaje += "<ul><a href=http://localhost:1234" + str(clave) + ">" +\
                           str(clave) + "</a>: " + str(valor) + "</ul>"

            url_qs = body.split("=")[1]  # Sacar url de la qs
            if url_qs == "":  # Con qs vacía en el campo url (formulario vacio)
                return ("400 Bad Request", respuesta_400.format(formulario=formulario, mensaje=mensaje))

            else:  # Si hay URL para acortar en la qs (formulario rellenado correctamente)
                contenido = unquote(url_qs)  # URL extraida de qs decodificada (utf-8 y sin simbolos)
                print("Cuerpo decodificado: " + contenido)
                # Manejo de inicios de url varios
                if contenido.startswith("http://www"):
                    contenido = "http://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("http://"):
                    contenido = "http://" + str(contenido.split("//")[-1])
                elif contenido.startswith("https://www"):
                    contenido = "https://" + str(contenido.split("www.")[-1])
                elif contenido.startswith("https://"):
                    contenido = "https://" + str(contenido.split("//")[1])
                else:
                    if contenido.startswith("www."):
                        # Se tiene en cuenta si empieza por www. para añadir https://
                        contenido = "https://" +\
                                    str(contenido.split("www.")[-1])
                    else:
                        # Casos con la url del sitio sin nada delante (se añade https://)
                        contenido = "https://" + contenido

                if contenido in urlDic_inverso:  # Si el contenido ya esta guardado, no necesita un recurso aleat nuevo
                    pass
                else:
                    # Se genera una cadena aleatoria para usar como recurso aleatorio en la URL acortada
                    recurso_aleat = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))
                    if recurso_aleat in urlDic_inverso.values():  # Comprueba que no exista ya el nuevo recurso aleat
                        recurso_aleat = ''.join(random.choices(string.ascii_lowercase + string.digits, k=10))
                    urlDic["/" + str(recurso_aleat)] = contenido  # Asigna al recurso aleatorio (ya con la /)
                    # de la URL acortada la URL real
                    urlDic_inverso[contenido] = str(recurso_aleat)  # Asigna a la URL real la acortada
                # Se tiene que hacer de nuevo para tener la lista con la ultima URL actualizada en la lista a mostrar
                    mensaje += "<ul><a href=http://localhost:1234/" + str(recurso_aleat) + ">/" +\
                               str(recurso_aleat) + "</a>: " + str(contenido) + "</ul>"

                return ("200 OK", respuesta_200_post.format(urlDic_inv_contenido=str(urlDic_inverso[contenido]),
                        contenido=contenido, formulario=formulario, mensaje=mensaje))


if __name__ == "__main__":
    try:
        testWebApp = Acortador_URLs("localhost", 1234)
    except KeyboardInterrupt:
        urlDic.close()
        urlDic_inverso.close()
        print("Cerrando acortador de Urls...")
